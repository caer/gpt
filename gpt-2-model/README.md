GPT-2 model taken from [OpenAI's official implementation](https://github.com/openai/gpt-2).

The model has been adapted for easy experimentation and export into ONNX format,
with modifications made to the original model source in order to 
resolve warnings related to deprecated Tensorflow 1.0 APIs.

## Building the Model(s)

To test and build the ONNX models, run:

1. `docker-compose build` (first time only)

2. `docker-compose up --build --attach-dependencies convert-gpt-2-tf-sm-to-onnx`

The first command needs to be run only once. The second command should be re-run
whenever model code is changed, or when a new model variant needs to be generated.

Once the command(s) complete, an [Onnx](https://onnxruntime.ai/) version of the model will be saved
to `saved_models`, along with the necessary hyper parameters to run the model. 

At the time of writing, the Docker Compose script is configured to only build 
the `124M` (i.e., 124 million parameter) version of the GPT-2 model,
which is the smallest version published by OpenAI. These models are hard-coded
to accept a batch size of `1` input token sequence, with a sequence length
of `128` tokens.

## License, Citation, and Contributions

New files and modifications are Copyright (C) 2022-24 Brandon Sanders [me@caer.cc];
all other content taken from the original GPT-2 OpenAI repository retains its original
copyright. Refer to [the license](./LICENSE.txt) for details.

Per the original GPT-2 author's requests, the following bibtex entry
should be included with any derivatives of this work:

```
@article{radford2019language,
  title={Language Models are Unsupervised Multitask Learners},
  author={Radford, Alec and Wu, Jeff and Child, Rewon and Luan, David and Amodei, Dario and Sutskever, Ilya},
  year={2019}
}
```

Contributions are always welcome!
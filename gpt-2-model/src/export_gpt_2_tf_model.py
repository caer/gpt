#!/usr/bin/env python3
# Copyright (c) 2022 Brandon Sanders [hello@crahda.cc]

import fire
import json
import os
import shutil
import numpy as np
import tensorflow as tf
import tensorflow.compat.v1 as tf1

import gpt_2_model as model
import gpt_2_sampler as sampler
import gpt_2_token_encoder as encoder

# Path where saved model data is stored.
BASE_SAVE_PATH = '/gpt-2/saved_models/'

# The number of distinct token sequences
# provided as input for inference.
#
# In ad-hoc use-cases, this value is likely
# to be `1`, since only one sequence (sentence)
# is provided at a time.
#
# During export, the Tensorflow model is
# configured with a batch size of `None`
# to allow downstream systems to configure
# it on startup.
BATCH_SIZE = 1

# The number of tokens in each distinct
# token sequence provided as input for inference.
TOKEN_SEQUENCE_LENGTH = 128

# If true, the input dimensions will be fixed
# to the constant values listed above; otherwise,
# they will be set to `None`, allowing downstream
# consumers of the ML model to specify them
# on startup.
#
# Fixed dimensions are required for some ML
# runtime libraries, like Sonos Tract's ONNX,
# to function properly.
FIX_INPUT_DIMS = True

# Exported name of the input to the model.
INPUT_TOKEN_SEQUENCES_NAME = 'token_sequences'

# Exported names of the output from the model.
OUTPUT_NEXT_TOKEN_INFERENCES = 'next_token_inferences'
OUTPUT_HIDDEN_LAYERS = 'hidden_layers'

def export_model(
    models_dir = 'models',
    model_name = '124M',
    random_seed = 3,
):
    """
    Exports headless GPT-2 in Tensorflow saved model format.
    """

    saved_model_path = BASE_SAVE_PATH + model_name

    # Load text encoder from filesystem.
    models_dir = os.path.expanduser(os.path.expandvars(models_dir))
    enc = encoder.get_encoder(model_name, models_dir)

    # Export vocabulary to saved model directory.
    encoder_path = os.path.join(models_dir, model_name, 'encoder.json')
    saved_encoder_path = saved_model_path + "_encoder.json"
    shutil.copy(encoder_path, saved_encoder_path)
    vocab_path = os.path.join(models_dir, model_name, 'vocab.bpe')
    saved_vocab_path = saved_model_path + "_vocab.bpe"
    shutil.copy(vocab_path, saved_vocab_path)

    # Load model training hyper parameters.
    hyper_parameters = model.default_hparams()
    hparams_path = os.path.join(models_dir, model_name, 'hparams.json')
    with open(hparams_path) as f:
        hyper_parameters.update(json.load(f))

        # Export hyper-parameters to saved model directory.
        saved_hparams_path = saved_model_path + "_hparams.json"        
        shutil.copy(hparams_path, saved_hparams_path)

    # Prepare Tensorflow graph
    with tf.compat.v1.Session(graph=tf.Graph()) as sess:
        np.random.seed(random_seed)
        tf.compat.v1.set_random_seed(random_seed)

        # Prepare inference inputs; if `None` is
        # passed as a dimension, Tensorflow will
        # treat that dimension as dynamically sized
        # and/or determined at runtime.
        context = None
        if FIX_INPUT_DIMS:
            context = tf.compat.v1.placeholder(tf.int32, [BATCH_SIZE, TOKEN_SEQUENCE_LENGTH])
        else:
            context = tf.compat.v1.placeholder(tf.int32, [None, None])

        # Prepare model via a fake invocation, which will
        # enable Tensorflow to build its graph.
        lm_output = model.model(hparams=hyper_parameters, X=context, past=None, reuse=tf.compat.v1.AUTO_REUSE)

        # Restore checkpointed model from the filesystem
        saver = tf.compat.v1.train.Saver()
        ckpt = tf.train.latest_checkpoint(os.path.join(models_dir, model_name))
        saver.restore(sess, ckpt)

        # Run tests.
        prompt = "GPT-2 is a machine learning model for natural language-processing;"
        print("Prompt: " + prompt, flush=True)
        next_tokens = enc.encode(prompt)[:TOKEN_SEQUENCE_LENGTH]
        output = prompt
        for i in range(2048):
            input_token_set = next_tokens.copy()

            # Right-pad input tokens with null tokens.
            attention_end_index = TOKEN_SEQUENCE_LENGTH - 1
            padding_length = TOKEN_SEQUENCE_LENGTH - len(input_token_set)
            for _ in range(padding_length):
                input_token_set.insert(0, 50256)

            # Run model with input.
            # Note: The odd syntax for context simply duplicates 
            #       the input to the model to simulate passing 
            #       a batch of `BATCH_SIZE`.
            model_output = sess.run(lm_output, feed_dict={
                context: [input_token_set for _ in range(BATCH_SIZE)],
            })

            # Post-process model output
            logits = model_output[0]
            presents = model_output[1]
            logits, presents = sampler.post_process_model_output(logits, presents, hyper_parameters, attention_end_index=attention_end_index)
            l2 = logits

            # l2 will contain an indice for each input_token_set in the batch
            next_word = enc.decode(l2[0])
            next_tokens.extend(l2[0])
            next_tokens = next_tokens[-TOKEN_SEQUENCE_LENGTH:]
            output += next_word

            # Stop making inferences once the model produces a period (full-stop).
            if output.endswith('.'):
                print("Response (took " + str(i) + " iterations) " + output)
                print("Exporting model to " + saved_model_path)
                print("Input (Token Sequence(s)): " + str(context))
                print("Output (Token Predictions): " + str(lm_output[0]))
                print("Output (Token Embeddings): " + str(lm_output[1]))

                # Export TF-2 saved model with:
                builder = tf1.saved_model.Builder(saved_model_path)
                sig_def = tf1.saved_model.predict_signature_def(
                    inputs={INPUT_TOKEN_SEQUENCES_NAME: context},
                    outputs={
                        # AKA "logits", shape=(
                        #   batch_size, 
                        #   token_sequence_length, 
                        #   vocabulary_size)
                        OUTPUT_NEXT_TOKEN_INFERENCES: lm_output[0],

                        # AKA "presents", shape=(
                        #   batch_size,
                        #   layers,
                        #   2 (key/value),
                        #   head_count,
                        #   token_sequence_length,
                        #   embeddings_per_layer / head_count)
                        OUTPUT_HIDDEN_LAYERS: lm_output[1],
                    })
                builder.add_meta_graph_and_variables(
                    sess, tags=["serve"], signature_def_map={
                        tf.saved_model.DEFAULT_SERVING_SIGNATURE_DEF_KEY: sig_def
                })
                builder.save()
                break

if __name__ == '__main__':
    fire.Fire(export_model)
#!/usr/bin/env python3
# Copyright (c) 2022 Brandon Sanders [hello@crahda.cc]

import numpy as np

def softmax(tensor, axis=-1):
    """
    Compute softmax values for each value in a tensor,
    accounting for underlfow and overflow as in
    https://stackoverflow.com/a/59111948
    """

    # Shift to handle underflow and overflow;
    # `tx.softmax` performs this shift implicitly:
    # https://github.com/tensorflow/tensorflow/blob/5dcfc51118817f27fad5246812d83e5dccdc5f72/tensorflow/core/kernels/softmax_op_functor.h#L86
    tensor = tensor - np.max(tensor, axis=axis, keepdims=True)

    # Softmax operation, which:
    #
    # 1. Calculates the exponent `ex` of Euler's constant (`e`)
    #    for each tensor value `tx`.
    # 2. Sum all `ex`; we'll call this result `sum(ex)`.
    # 3. Calculate `ex / sum ex` for every `ex`, which will
    #    equal the normalized probability of each corresponding `x`.
    #
    # Many thanks to https://medium.com/analytics-vidhya/understanding-the-gpt-2-source-code-part-1-4481328ee10b,
    # and https://stats.stackexchange.com/questions/289369/log-probabilities-in-reference-to-softmax-classifier,
    # and https://deepai.org/machine-learning-glossary-and-terms/softmax-layer
    numerator = np.exp(tensor)
    denominator = np.sum(numerator, axis=axis, keepdims=True)
    softmax = numerator / denominator

    return softmax

def sample_nucleus(tensor, p=1):
    """
    Based in part around the original OpenAI nucleus sampling PR:
    https://github.com/openai/gpt-2/pull/133/files
    As opposed to the implementation merged into their main branch:
    https://github.com/openai/gpt-2/commit/ac5d52295f8a1c3856ea24fb239087cc1a3d1131
    """

    batch_size, token_vocabulary_size = tensor.shape

    # Sort all values in descending order.
    sorted_tensor = -np.sort(-tensor, axis=-1)

    # Take the softmax of the input tesnor
    # to convert all values to a 0 - 1 log-normalized
    # probability distribution that sums to 1.
    sorted_tensor_sm = softmax(sorted_tensor, axis=-1)

    # Calculate cumulative sums, so we can identify the minimum
    # number of values, starting from index `0`, that together
    # have `>= p` normalized probability.
    #
    # This set of values represents `k`.
    sorted_tensor_sm_cs = np.cumsum(sorted_tensor_sm, axis=-1)

    # Calculate the number of values in `k`, which
    # corresponds to the index of the lowest probability
    # value within `k`.
    #
    # We subtract `1` from each `k` to convert it
    # to an index representing the index of the last
    # value in each k in the sorted tensor.
    sorted_tensor_k_min_index = np.sum(sorted_tensor_sm_cs <= p, axis=-1) - 1

    # Ensure all `k` indexes are at least `0`.
    sorted_tensor_k_min_index = np.where(
        sorted_tensor_k_min_index >= 0,
        sorted_tensor_k_min_index,
        0
    )

    # Gather all `k` min values from the sorted tensor.
    sorted_tensor_k_min_value = np.zeros((batch_size, 1))
    for batch in range(batch_size):
        sorted_tensor_k_min_value[batch] = sorted_tensor[batch, sorted_tensor_k_min_index[batch]]

    # Mask all values in the original unsorted tensor `< min(k)`,
    # ensuring only values within `k` will be unmasked in the result,
    # thus completing the sampling.
    masked_tensor = np.where(
        tensor < sorted_tensor_k_min_value,
        np.ones_like(tensor) * -1e10,
        tensor
    )

    return masked_tensor

def post_process_model_output(logits, presents, hyper_parameters, attention_end_index=-1):
    """
    Applies post-processing steps to the raw output of a GPT-2 model.
    """

    # Extract raw logits data, and trim off
    # any vocabulary in excess of the one supported
    # by these hyper parameters.
    batch_size, token_set_length, vocabulary_size = logits.shape
    logits = logits[:, :, :hyper_parameters["n_vocab"]]

    # Select the final tokens from each batch that our attention
    # is directed to.
    #
    # TODO: attention_end_index will be different per-batch entry.
    logits = logits[:, attention_end_index, :]

    # Apply temperature gradient to logits; this "temperature"
    # controls the entropy of selected logits. T=0 will result
    # in no entropy, T=1 will defer to the model's internal
    # entropy, and T>1 will exaggerate the model's entropy.
    #
    # In general, higher T values result in more "creative"
    # model results.
    temperature = 0.9
    logits = logits / temperature

    # Pre-sample each batch with nucleus sampling to
    # restrict the final sample to P most probable
    # tokens. P=1 will choose from the entire model vocabulary,
    # and P=0 will select only the most likely word.
    #
    # In general, lower P values will result in
    # more "creative" model results.
    logits = sample_nucleus(logits, p=0.5)

    # Sample a recommended token for each batch.
    return_logits = np.zeros((batch_size, 1))
    for batch in range(batch_size):

        # Convert the logits into a normalized distribution
        # via softmax. tf.multinomial does this operation
        # internally, but other frameworks that sample a
        # multinomial/categorical distribution do not.
        batch_logits = softmax(logits[batch])

        # Cast to floats to handle rounding issues in numpy's
        # multinomial method.
        # https://github.com/numpy/numpy/issues/11847
        batch_logits = batch_logits.astype(float)
        batch_logits = batch_logits / np.sum(batch_logits)

        # Perform multinomial sampling on batch logits,
        # using argmax to select the index of the sampled
        # (selected) token; it will have a value of `1`,
        # and all others will be `0`.
        sampled_token_index = np.argmax(np.random.multinomial(1, batch_logits))
        return_logits[batch, 0] = sampled_token_index

    # Returned logits will have the shape [batch_size, 1], with each
    # batch containing exactly one predicted token.
    #
    # Presents will have the shape [batch_size, 2, head_count, 
    # tokens_per_set, embedding_length // head_count]
    return [return_logits, presents]
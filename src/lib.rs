//! Pure Rust inference wrapper for the GPT-2
//! (and possibly later) model family.

pub mod model;
pub mod tokenizer;
